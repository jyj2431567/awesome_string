//
// Created by drewjin on 2023/10/17.
//
#include "_string.h"
void testfind()
{
    _string str1("Neymar");//1
    size_t pos1 = str1.find("y",2);
    std::cout << "pos1 = str1.find(\"y\",2): " << pos1<<std::endl;

    _string str2("abcdefg");//2
    size_t pos2 = str2.find("ef", 2);
    std::cout << "pos2 = str2.find(\"ef\", 2): " << pos2 << std::endl;

    _string str3("helloworld!");//3
    size_t pos31 = str3.find("wo", 2,6);
    std::cout << "pos31 = str3.find(\"wo\", 2,6): " << pos31 << std::endl;
    size_t pos32 = str3.find("wo", 1, 2);
    std::cout << "pos32 = str3.find(\"wo\", 1, 2): " << pos32 << std::endl;

    _string teststring("and");
    _string str4 = ("Neymar and Messi");
    size_t pos4=str4.find(teststring, 3);
    std::cout << "pos4=str4.find(teststring, 3): " << pos4 << std::endl;
}