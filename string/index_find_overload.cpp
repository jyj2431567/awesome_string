#include "_string.h"

// 下标取值重载
char & _string::operator[](const size_t &index) const {
    return *(_start + index);
}