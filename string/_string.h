//
// Created by drewjin on 2023/10/13.
//

#ifndef STRING_STRING_H
#define STRING_STRING_H

#include <iostream>
#include <cstdio>
#include <cstring>

class _string {
    public:
        typedef char* iterator;
        typedef const char* const_iterator;

        // 构造函数，析构函数
        _string();                                          //1
        _string(const size_t &n, const char &ch = char());  //1
        _string(const char &ch);                            //1
        _string(const char *cstr);                          //1
        _string(const _string &str);                        //1
        ~_string();                                         //1

        // 赋值重载、取值重载
        char & operator[](const size_t &index) const;       //1
        _string & operator=(const _string &str);            //1

        // 输入输出重载
        friend std::ostream & operator<<(std::ostream & os, const _string &str);
        friend std::istream & operator>>(std::istream & is, _string &str);

        // 运算重载
        friend _string &operator+(const _string &str1, const _string &str2);
        friend _string & operator+(const _string & str, const char &ch);
        friend _string & operator+(const _string & str, const char *cstr);
        _string & operator+=(const _string &str);
        _string & operator+=(const char &ch);
        _string & operator+=(const char *cstr);

        friend _string &operator*(const _string &str, const size_t &n);
        _string & operator*=(const size_t &n);

        // 迭代器调用函数
        iterator begin() { return _start; }
        iterator end() { return _end; }
        iterator cbegin() const { return _start; }
        iterator cend() const { return _end; }

        // 内存分配函数
        size_t size() const { return _end - _start; }               //1
        size_t capacity() const { return _endofstorage - _start; }  //1
        void reserve(const size_t &n);
        void resize(const size_t &n, const char &ch = char());
        void clear();   //  清除字符串中的全部元素。

        // 增删改查函数
        void push_back(const char &ch);
        void append(const char &ch);
        void pop_back();    //  删除字符串的最后一个元素。

        //  从字符串中的指定位置删除一个或一系列元素。
        //  first:  一种迭代器，用于寻址要清除范围中的第一个元素的位置。
        //  last:   一种迭代器，用于寻址要清除范围中最后一个元素之后下一个元素的位置。
        //  iter:   一种迭代器，用于寻址要清除字符串中的元素位置。
        //  offset: 要删除的字符串中的第一个字符的索引。
        //  cnt:  如果在字符串范围中有同样数量的以 offset 开头的元素，将删除该元素数目。
        iterator erase(iterator first, iterator last);
        iterator erase(iterator iter);
        iterator erase(const size_t &offset = 0, const size_t &cnt = 10);//

        //  将一个、多个或一系列元素插入到指定位置的字符串中。
        //  index:  插入点之后的位置索引
        //  offset: 提供要追加的字符的源字符串部分的索引
        //  cnt:    要插入的字符数
        _string &insert(const size_t &index, const char *cstr);
        _string &insert(const size_t &index, const char *cstr, const size_t &cnt);
        _string &insert(const size_t &index, const _string &str);
        _string &insert(const size_t &index, const _string &str, const size_t &offset, const size_t &cnt);
        _string &insert(const size_t &index, char &ch);
        iterator insert(iterator iter, const char *cstr);
        iterator insert(iterator iter, const char &ch);


        char & at(const size_t &offset);      //  返回对字符串中指定位置的元素的引用。

        _string substr(const size_t &offset = 0, const size_t &cnt = 10) const;  //  从字符串起始处的指定位置复制最多某个数目的字符的子字符串。

        // char_value:  成员函数要搜索的字符值。
        // offset:      搜索开始处的索引。
        // cstr:         成员函数要搜索的 C 字符串。
        // cnt:       在成员函数要搜索的 C 字符串中从第一个字符开始计数的字符数。
        // str:         成员函数要搜索的字符串。
        size_t find(const char &ch, const size_t &offset = 0) const;    //  向前搜索字符串，搜索与指定字符序列匹配的第一个子字符串。
        size_t find(const char *cstr, const size_t &offset = 0) const;
        size_t find(const char *cstr, const size_t &offset, const size_t &count) const;
        size_t find(const _string &str, const size_t &offset = 0) const;

        bool empty();   //  测试字符串是否包含字符。

        size_t length() const;

        const char * data() const;    //  将字符串的内容转换为字符数组。
        char *data();
        const char * c_str() const;   //  将字符串的内容转换为以 null 结尾的 C 样式字符串。
    private:
        iterator _start;
        iterator _end;
        iterator _endofstorage;//

};

#endif //STRING_STRING_H
