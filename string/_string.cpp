#include "_string.h"

// 构造析构
_string::_string(): _start(nullptr), _end(nullptr), _endofstorage(nullptr) { }
_string::_string(const size_t &n, const char &ch): _start(nullptr), _end(nullptr), _endofstorage(nullptr) {
    resize(n, ch); 
}
_string::_string(const char &ch): _start(nullptr), _end(nullptr), _endofstorage(nullptr) {
    push_back(ch); 
}
_string::_string(const char *cstr): _start(nullptr), _end(nullptr), _endofstorage(nullptr) {
    for(int i = 0; i < strlen(cstr); ++i) push_back(cstr[i]); 
}
_string::_string(const _string &str): _start(nullptr), _end(nullptr), _endofstorage(nullptr) {
    reserve(str.capacity());
    const_iterator it = str.cbegin();
    const_iterator it_end = str.cend();
    iterator dst = _start;
    while(it != it_end) {
        *dst = *it;
        dst++; it++;
    } _end = dst;
}
_string::~_string() {
    delete [] _start; 
}

// 内存操作
void _string::reserve(const size_t &n) {
    if(n > capacity()) {
        size_t _size = size();
        iterator temp = new char [n];
        for(size_t i = 0; i < _size; ++i) {
            temp[i] = _start[i];
        } delete [] _start;
        _start = temp;
        temp = nullptr;
        _end = _start + _size;
        _endofstorage = _start + n;
    } else return;
}
void _string::resize(const size_t &n, const char &ch) {
    if(n > capacity()) {
        reserve(n);
        iterator new_end = _start + n;
        while(_end != new_end) {
            *_end = ch;
            _end++;
        }
    } else _end = _start + n;
}
void _string::clear() {
    delete [] _start;
    _start = nullptr;
    _end = nullptr;
    _endofstorage = nullptr;
}