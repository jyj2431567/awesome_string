#include "_string.h"

// 乘法
_string &operator*(const _string &str, const size_t &n)
{
    static _string temp;
    temp.clear();
    _string::iterator it;
    for (size_t times = 1; times <= n;times++)
    {
        for (it = str.cbegin(); it != str.cend(); ++it)
        {
            temp.push_back(*it);
        }
    }
    return temp;
}
_string & _string::operator*=(const size_t &n)   //a*=b  即  a=a*b
{
    *this = *this * n;
    return *this;
}
