#include "_string.h"
void testOstream();
void test03();
void test1() {


    test03();
    /*testOstream();
	_string str1, str2('a'), str3(2, 'a'), str4("jyj"),str100("abcdefghijkl"),str101("1234567890"),str102("hello,world");
	std::cout << "str1: " << str1 << std::endl
              << "str2: " << str2 << std::endl
              << "str3: " << str3 << std::endl
              << "str4: " << str4 << std::endl;

	_string str5(str4);
	std::cout << "str5: " << str5 << std::endl;

	str1 = str5;
    str1.pop_back();
	std::cout << "str1 (str1 = str5): " << str1 << std::endl;

    std::cout << "str5.c_str(): " << str5.c_str() << std::endl;
    std::cout <<"str5.data(): "<< str5.data() << std::endl;
    str5.insert(3,"nb");
    std::cout << "str5 (insert(2,nb)): " << str5 << std::endl;


	str5.push_back('a');
	std::cout << "str5 (push_back('a')): " << str5 << std::endl;
    str5.insert(0,str4);
    std::cout << "str5 (insert(0,str4)): " << str5 << std::endl;
    str5.insert(str5.end(),'a');
    std::cout<<"str5 (insert(end(),'a')): "<<str5<<std::endl;

    std::cout<<"origin = "<<str100;
    str100.erase(str100.begin()+2,str100.end()-2);
    std::cout<<"\terase_test1:"<<str100<<std::endl;

    std::cout<<"origin = "<<str101<<"   ";
    str101.erase(str101.begin());
    std::cout<<"\terase_test2:"<<str101<<std::endl;

    std::cout<<"origin = "<<str102;
    str102.erase(str102.erase(2,3));
    std::cout<<"\terase_test3:"<<str102<<std::endl;

	_string str6 = str5 + str1;
	std::cout << "str6 (str6 = str5 + str1): " << str6 << std::endl;

	_string str7;
	std::cin >> str7;
	std::cout << "str7 (cin): " << str7 << std::endl;

	str6 += str7;
	std::cout << "str6 (str6 += str7): " << str6 << std::endl;
	std::cout << "str6[2]: " << str6[2] << std::endl;

    str6 += "jyj";
    std::cout << "str6 (str6 += \"jyj\"): " << str6 << std::endl;

    str6 += 'j';
    std::cout << "str6 (str6 += 'j'): " << str6 << std::endl;*/

}
void testOstream(){
    _string str("Hello,World");
    std::cout<<"输出重载:"<<"\t "<<str<<std::endl;
}
void testAdd(){

}

void test03(){
    _string str1("Hello"),str2("World");
    //输出重载测试
    std::cout<<"输出重载:"<<std::endl;
    std::cout<<"\t"<<"str1: "<<str1<<std::endl;
    std::cout<<"\t"<<"str1: "<<str2<<std::endl;

    std::cout<<"运算符重载："<<std::endl;
    //加法重载 1
    std::cout<<"\t+重载："<<std::endl;
    std::cout<<"\t\t"<<"str1 +  str2: "<<str1+str2<<std::endl;
    //加法重载 2
    _string tmp1 = str1+'A';
    std::cout<<"\t\t"<<"str1 +  'A': "<<tmp1<<std::endl;
    //加法重载 3
    _string tmp2 = str1+str2.begin();
    std::cout<<"\t\t"<<"str1 +  str2.begin(): "<<tmp2<<std::endl;

    std::cout<<"\t+=重载："<<std::endl;
    //+=重载1
    _string tmp = str1 += str2;
    std::cout<<"\t\t"<<"str1 += str2: "<<tmp<<std::endl;
    //+=重载2
     tmp = str1 += 'a';
    std::cout<<"\t\t"<<"str1 += 'a': "<<tmp<<std::endl;
    //+=重载3
     tmp = str1 += str2.begin();
    std::cout<<"\t\t"<<"str1 += str2.begin(): "<<tmp<<std::endl;

    std::cout<<"\t*重载："<<std::endl;
    //*重载
    str1="hello";
     tmp = str1*2;
    std::cout<<"\t "<<"str1*2: "<<tmp<<std::endl;


    std::cout<<"\t*=重载："<<std::endl;
    //*=重载1
    str1 = "hello";
    str1 *= 2;
    std::cout<<"\t "<<"str1 *= 2: "<<str1<<std::endl;

    //增删改查函数测试
    std::cout<<"增删改查函数:"<<std::endl;

    std::cout<<"    push_back测试："<<std::endl;
    _string t01 = "helloWorld";
    std::cout<<"        t01 = "<<t01<<std::endl;
    t01.push_back('!');
    std::cout<<"        t01.push_back('!')："<<t01<<std::endl;

    //append
    std::cout<<"    append测试："<<std::endl;
    _string t02 = "helloWorld";
    std::cout<<"        t02 = "<<t02<<std::endl;
    t02.append('!');
    std::cout<<"        t01.append('!')："<<t02<<std::endl;

    //pop_back
    std::cout<<"    pop_back测试："<<std::endl;
    _string t03 = "helloWorld";
    std::cout<<"        t03 = "<<t03<<std::endl;
    t03.pop_back();
    std::cout<<"        t03.pop_back()："<<t03<<std::endl;

    //erase
    std::cout<<"\terase测试："<<std::endl;
    _string t04("abcde"),t05("abcde"),t06("abcde");
    std::cout<<"\t\tt04 = abcde"<<std::endl;
    std::cout<<"\t\tt04.erase(t04.begin()): "<<t04.erase(t04.begin())<<std::endl;
    std::cout<<std::endl<<"\t\tt05 = abcde"<<std::endl;
    t05.erase(t05.begin()+1,t05.end()-1);
    std::cout<<"\t\tt05.erase(t05.begin()+1,t05.end()-1): "<<t05<<std::endl;
    std::cout<<std::endl<<"\t\tt06 = abcde"<<std::endl;
    t06.erase(2,2);
    std::cout<<"\t\tt06.erase(2,2): "<<t06<<std::endl;

    //insert
    std::cout<<std::endl<<"\tinsert:"<<std::endl;
    _string t07("ABCDE");
    t07.insert(1,'1');
    std::cout<<"\t\tt07: ABCDE"<<std::endl<<"\t\tt07.insert(1,'1'): "<<t07<<std::endl;

    _string t08("ABC"),t09("DEF");
    t08.insert(2,t09);
    std::cout<<std::endl<<"\t\tt08 = ABC\t"<<"t09 = DEF"<<std::endl<<"\t\tt08.insert(2,t09): "<<t08<<std::endl;

    //find
    std::cout<<std::endl<<"find函数测试"<<std::endl;
    _string t10("Neymar");//1
    size_t pos1 = t10.find("y",2);
    std::cout<<"\tt10 = "<<t10<<std::endl;
    std::cout << "\tpos1 = t10.find(\"y\",2): " << pos1<<std::endl;

    _string t11("abcdefg");//2
    std::cout<<std::endl<<"\tt11 = "<<t11<<std::endl;
    size_t pos2 = t11.find("ef", 2);
    std::cout << "\tpos2 = t11.find(\"ef\", 2): " << pos2 << std::endl;

    _string t12("helloworld!");//3
    size_t pos3_1 = t12.find("wo", 2,6);
    std::cout<<std::endl<<"\tt12 = "<<t12<<std::endl;
    std::cout << "\tpos3_1 = t12.find(\"wo\", 2,6): " << pos3_1 << std::endl;
    size_t pos3_2 = t12.find("wo", 1, 2);
    std::cout << "\tpos3_2 = t12.find(\"wo\", 1, 2): " << pos3_2 << std::endl;

    _string teststring("and");
    _string t13 = ("Neymar and Messi");
    std::cout<<std::endl<<"\tt13 = "<<t13<<std::endl;
    size_t pos4=t13.find(teststring, 3);
    std::cout << "\tpos4=t13.find(teststring, 3): " << pos4 << std::endl;

    //empty
    std::cout<<std::endl<<"empty函数:"<<std::endl;
    _string t14('1');
    std::cout<<"t14 = "<<t14<<" isEmpty?: ";
    std::cout<<t14.empty();

    //len
    std::cout<<std::endl<<std::endl<<"length函数:"<<std::endl;
    _string t15("12345");
    std::cout<<"t15 = "<<t15<<" length?: ";
    std::cout<<t15.length();

    //data
    _string str4("hello");
    _string str5(str4);
    std::cout << "str5: " << str5 << std::endl;
    std::cout << "str5.c_str(): " << str5.c_str() << std::endl;
    std::cout <<"str5.data(): "<< str5.data() << std::endl;




std::cout<<std::endl<<"分界线"<<std::endl;
}


