#include "_string.h"

_string::iterator _string::erase(_string::iterator first, _string::iterator last) {
    // 检查传入的迭代器是否有效
    if (first < _start || last > _end || first > last) {
        return _start;
    }

    size_t numElementsToRemove = last - first;
    size_t newSize = size() - numElementsToRemove;

    for (iterator src = last, dst = first; src != _end; ++src, ++dst) {
        *dst = *src;
    }

    // 清空多余的字符
    for (iterator dst = _start + newSize; dst != _end; ++dst) {
        *dst = '\0';
    }

    _end = _start + newSize;
    return first;
}


/*
_string::iterator _string::erase(_string::iterator first, _string::iterator last) {
    // 检查传入的迭代器是否有效
    if (first < _start || last > _end || first > last) {
        return _start;
    }
    size_t numElementsToRemove = last - first;
    size_t newSize = size() - numElementsToRemove;
    for (iterator src = last, dst = first; src != _end; ++src, ++dst) {
        *dst = *src;
    }
    _end = _start + newSize;
    return first;
}
*/

/*
_string::iterator _string::erase(_string::iterator first, _string::iterator last) {
    // 检查传入的迭代器是否有效
    if (first < _start || last > _end || first > last) {
        return _start;
    }

    size_t numElementsToRemove = last - first;
    size_t newSize = size() - numElementsToRemove;

    for (iterator src = last, dst = first; src != _end; ++src, ++dst) {
        *dst = *src;
    }

    // 清空被删除字符后的位置
    for (iterator dst = _start + newSize; dst != _end; ++dst) {
        *dst = '\0';
    }

    _end = _start + newSize;
    return first;
}
*/


_string::iterator _string::erase(_string::iterator iter) {
    if (iter < _start || iter >= _end) {
        return iter;
    }
    for (iterator src = iter + 1, dst = iter; src != _end; ++src, ++dst) {
        *dst = *src;
    }
    --_end;
    *(_end) = '\0';
    return iter;
}



_string::iterator _string::erase(const size_t &offset, const size_t &cnt) {
    if (offset >= size() || cnt == 0) {
        return _start;
    }
    size_t index = offset;
    size_t end = offset + cnt;
    for (iterator src = _start + end, dst = _start + index; src != _end; ++src, ++dst) {
        *dst = *src;
    }
    _end = _start + size() - cnt;
    return _start + index;
}
