#include "_string.h"

// 输入输出重载
std::ostream & operator<<(std::ostream & os, const _string &str) {
    for (_string::iterator it = str.cbegin(); it != str.cend(); ++it) { os << *it; }
    return os;
}
std::istream & operator>>(std::istream & is, _string &str) {
    char ch;
    while(true) {
        is.get(ch);
        if(ch == '\n') return is;
        str.push_back(ch);
    }
}