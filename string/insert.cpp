#include "_string.h"

_string & _string::insert(const size_t &index, const char *cstr) {
    _string temp;
    temp.reserve(size() + strlen(cstr) + 1);
    reserve(size() + strlen(cstr) + 4);
    for(size_t i = 0; i < index; ++i) {
        temp.push_back(_start[i]);
    }
    for(size_t i = 0; i < strlen(cstr); ++i) {
        temp.push_back(cstr[i]);
    }
    for(size_t i = index; i < size(); ++i) {
        temp.push_back(_start[i]);
    }
    for(size_t i = 0; i < size()+strlen(cstr); ++i) {
        _start[i] = temp[i];
    }
    _start[size() + strlen(cstr)] = '\0';
    _end = _start + size() + strlen(cstr);
    _endofstorage = _start + size() + strlen(cstr)+4;
    return *this;
}
_string & _string::insert(const size_t &index, const char *cstr, const size_t &cnt) {
    if(cnt >= strlen(cstr))
    {
        insert(index, cstr);
    }
    else
    {
        char *temp = new char[cnt];
        for (size_t i=0;i<cnt;i++)
        {
            temp[i]=cstr[i];
        }
        insert(index,temp);
    }

    return *this;
}
_string & _string::insert(const size_t &index, const _string &str) {
    insert(index,str.c_str());
    return *this;
}
_string & _string::insert(const size_t &index, const _string &str, const size_t &offset, const size_t &cnt) {
    new char [str.size()];
    const char *tem = str.c_str();
    new char [cnt];
    char *tem2;
    for(size_t n=offset; n<cnt;++n)
    {
        tem2[n]=tem[n];
    }
    insert(index,tem2);
    return *this;
}
_string & _string::insert(const size_t &index,  char &ch) {
    insert(index,&ch);
    return *this;
}
_string::iterator _string::insert(_string::iterator iter, const char *cstr)
{
_string temp;
    temp.reserve(size() + strlen(cstr) + 1);
    reserve(size() + strlen(cstr) + 4);
    for(size_t i = 0; i < iter - _start; ++i) {
        temp.push_back(_start[i]);
    }
    for(size_t i = 0; i < strlen(cstr); ++i) {
        temp.push_back(cstr[i]);
    }
    for(size_t i = iter - _start; i < size(); ++i) {
        temp.push_back(_start[i]);
    }
    for(size_t i = 0; i < size()+strlen(cstr); ++i) {
        _start[i] = temp[i];
    }
    _start[size() + strlen(cstr)] = '\0';
    _end = _start + size() + strlen(cstr);
    _endofstorage = _start + size() + strlen(cstr)+4;
    return iter;
}
_string::iterator _string::insert(_string::iterator iter, const char &ch) {
    char *cstr = new char[2];
    cstr[0] = ch;
    cstr[1] = '\0';
    insert(iter,cstr);
    return iter;
}