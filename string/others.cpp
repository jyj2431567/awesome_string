#include "_string.h"

// 增删改查函数
void _string::push_back(const char &ch) {
    if (ch < 0 || ch > 127) return; // 不是ASCII码则退出
    size_t _size = size(), _capacity = capacity();
    if(_capacity == 0) {
        reserve(8);
    } else if(_size == _capacity) {
        reserve(2 * _capacity);
    }
    *_end = ch;
    _end++;
}
void _string::append(const char &ch) {
    push_back(ch);
}
void _string::pop_back() {
    size_t _size = size(), _capacity = capacity();
    if(_size == 0) return;
    else if(_size == 1) {
        clear();
    } else if(_size == _capacity)
    {
        _end--;
    }
    else if(_capacity>16 && _size <= _capacity / 4) {
        reserve(_capacity / 2);
        _end--;
    } else _end--;
}       //  删除字符串的最后一个元素。
char & _string::at(const size_t &offset) {
    return *(_start+offset);
}

_string _string::substr(const size_t &offset, const size_t &cnt) const {
    // 待完成，此处书写用于正常测试
    _string str;
    return str;
}


bool _string::empty() {
    size_t _size = size();
    if(_size == 0)
        return true;
    else
        return false;
}

size_t _string::length() const {
    size_t len = size();
    return len;
}

const char* _string::data() const
{
    char* cstr = new char[size() + 1];
    for (size_t i = 0; i < size(); i++) {
        cstr[i] = _start[i];
    } cstr[size()] = '\0';
    return cstr;
}

char * _string::data()
{
    char* cstr = new char[size() + 1]; // 使用 new[] 分配动态内存
    for(size_t i = 0; i < size(); i++) {
        cstr[i] = _start[i];
    }
    cstr[size()] = '\0';
    return cstr;
}

const char * _string::c_str() const {
    char *cstr = new char[size() + 1]; // 使用 new[] 分配动态内存

    for (int i = 0; i < size(); i++) {
        cstr[i] = _start[i];
    }
    cstr[size()] = NULL;
    return cstr;
}

