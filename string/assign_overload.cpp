#include "_string.h"

// 深赋值重载
_string & _string::operator=(const _string &str) {
    clear();
    reserve(str.capacity());
    const_iterator it = str.cbegin();
    const_iterator it_end = str.cend();
    iterator dst = _start;
    while(it != it_end) {
        *dst = *it;
        dst++; it++;
    } _end = dst;
    return *this;
}