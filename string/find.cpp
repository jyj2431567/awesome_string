#include "_string.h"
#include<algorithm>
 
size_t _string::find(const char& ch, const size_t& offset) const
{
    {
        const_iterator start = cbegin() + offset;
        const_iterator result = std::find(cbegin() + offset, cend(), ch);

        if (result != cend())
        {
            return result - cbegin(); // 计算找到字符的序号
        }
        else
        {
            std::cout << "this char cannot be found" << std::endl;
            return -1; // 返回值为-1表示未找到字符
        }
    }
}

size_t _string::find(const char *cstr, const size_t &offset) const 
{ 
    size_t len = strlen(cstr);
    for (size_t i = offset; i <= size() - len; ++i) 
    {
        bool found = true;
        for (size_t j = 0; j < len; ++j) 
        {
            if (_start[i + j] != cstr[j])
            {
                found = false;
                break;
            }
        }
        if (found)
        {
            return i; 
        }
    }
    //能到这就是没找到
    std::cout << "this char cannot be found" << std::endl;
    return -1; 
} 


size_t _string::find(const char *cstr, const size_t &offset, const size_t &count) const
{ 
        size_t len = strlen(cstr);
        size_t endnumber = std::min(size(), offset + count);
        for (size_t i = offset; i <= endnumber - len; ++i)
        {
            bool found = true;
            for (size_t j = 0; j < len; ++j)
            {
                if (_start[i + j] != cstr[j]) 
                {
                    found = false;
                    break;
                }
            }
            if (found) 
            {
                return i;
            }
        }
        std::cout << "this char cannot be found" << std::endl;
        return -1;
    }

size_t _string::find(const _string &str, const size_t &offset) const { 
        size_t stringlength = str.size();
        size_t endnumber = size() - stringlength;

        for (size_t i = offset; i <= endnumber; ++i) {
            bool found = true;
            for (size_t j = 0; j < stringlength; ++j) {
                if (_start[i + j] != str[i]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return i;
            }
        }
        std::cout << "this string cannot be found" << std::endl;
        return -1;
    }

