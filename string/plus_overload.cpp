#include "_string.h"

// 加法
_string & operator+(const _string &str1, const _string &str2) {
    static _string temp;
    temp.clear();
    _string::iterator it;
    for(it = str1.cbegin(); it != str1.cend(); ++it) temp.push_back(*it);
    for(it = str2.cbegin(); it != str2.cend(); ++it) temp.push_back(*it);
    return temp;
}
_string & operator+(const _string & str, const char &ch) {
    static _string temp;
    temp.clear();
    temp = str;
    temp.push_back(ch);
    return temp;
}
/*_string & operator+(_string &str, char ch) {
    str.push_back(ch);  // 直接在原始字符串上添加字符
    return str;
}*/

_string & operator+(const _string & str, const char *cstr) {
    static _string temp1;
    temp1.clear();
    temp1 = str;
    _string temp2(cstr);
    int cnt = 0;
    for(int i = 0; i < strlen(cstr); ++i) {
        temp1.push_back(cstr[i]);
        if (cstr[i] < 0 || cstr[i] > 127) cnt++;
        if(cnt > 2) break;
    } return temp1;
}

/*_string & operator+(_string const& str, const char* cstr) {
    _string result = str;  // 复制原始字符串
    for (int i = 0; cstr[i] != '\0'; ++i) {
        result.push_back(cstr[i]);
    }
    return result;
}*/


_string & _string::operator+=(const _string &str) {
    for(iterator it = str.cbegin(); it != str.cend(); ++it) { push_back(*it); }
    return *this;
}
_string & _string::operator+=(const char &ch) {
    push_back(ch);
    return *this;
}
_string & _string::operator+=(const char *cstr) {
    for(int i = 0; i < strlen(cstr); ++i) { push_back(cstr[i]); }
    return *this;
}